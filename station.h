#pragma once
#include <iostream>
#include <mutex>
#include "train.h"

class Train;

class Station
{
public:

    Station();

    ~Station() = default;

    void setCurrentTrain(Train* train);

    void releaseTrain();

    Train* getStationStatus();

private:

    Train* current_train;
    std::mutex train_operation;
};