
#include "train.h"
#include "station.h"
#include <thread>
#include <chrono>


    Train::Train(Station* target, int time, std::string& inName) : station(target), travel_time(time), name(inName) {};

    void Train::movement() {
        std::this_thread::sleep_for(std::chrono::seconds(travel_time));
        station->setCurrentTrain(this);
    }

    std::string Train::getName() {
        return name;
    }