#include <iostream>
#include <thread>
#include <string>
#include "train.h"
#include "station.h"

int main() {

    const int trains_count = 3;
    std::string train_name[trains_count] = { "Train_A", "Train_B", "Train_C"};
    Train* trains[trains_count];

    Station* station = new Station();

    for (int i = 0; i < trains_count; i++) {
        std::cout << "Input the travel time of " << train_name[i] << " (s):";
        int time;
        std::cin >> time;
        trains[i] = new Train(station, time, train_name[i]);
    }

    std::cout << "Starting movement." << std::endl;
    for (int i = 0; i < trains_count; i++) {
        std::thread train_go(&Train::movement, trains[i]);
        train_go.detach();
    }

    int count = trains_count;

    while (count > 0) {
        if (station->getStationStatus()) {
            Train* arrived = station->getStationStatus();

            std::string order;
            do {
                std::cout << arrived->getName() << " has arrived and waiting your order (depart):";
                std::cin >> order;
            } while (order != "depart");

            std::cout << arrived->getName() << " left the station." << std::endl;
            station->releaseTrain();
            count--;
        }
    }

    for (int i = 0; i < trains_count; i++) {
        delete trains[i];
    }

    delete station;

    return 0;
}
