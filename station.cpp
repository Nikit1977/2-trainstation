
#include "station.h"
#include "train.h"

    Station::Station() : current_train(nullptr) {}

    void Station::setCurrentTrain(Train* train)
    {
        if (current_train) {
            std::cout << "\n" << train->getName() << " is waiting for a free path." << std::endl;
        }

        train_operation.lock();
        current_train = train;
    }

    void Station::releaseTrain() {
        current_train = nullptr;
        train_operation.unlock();
    }

    Train* Station::getStationStatus() {
        return current_train;
    }

