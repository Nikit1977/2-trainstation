
#pragma once
#include <string>
#include "station.h"

class Station;

class Train
{
    int travel_time;
    Station* station;
    std::string name;

public:
    Train(Station* target, int time, std::string& inName);

    ~Train() = default;

    void movement();

    std::string getName();
};
